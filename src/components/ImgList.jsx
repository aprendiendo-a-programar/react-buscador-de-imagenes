import Img from './Img';

const ImgList = ({ img }) => {
    return (
        <div className="col-12 p-5 row">
            {
                img.map((image) => {
                    return(
                        <Img 
                            key={img.id}
                            image={image}
                        />
                    )
                })
            }
        </div>
    );
}
 
export default ImgList;