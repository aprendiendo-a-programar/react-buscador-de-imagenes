import { useState, useEffect } from 'react';
import { Form, ImgList } from './components/index';
import './App.css';

function App() {
  const [search, saveSearch] = useState('');
  const [img, saveImg] = useState([]);
  const [actualPage, saveActualPage] = useState(1);
  const [totaPages, saveTotalPages] = useState(1);

  useEffect(() => {

    const consultApi = async () => {
      if (search === '') {
        return;
      }

      const imgPerPage = 30;
      const key = '22411923-22353b1ba36576f05fed36647';
      const url = `https://pixabay.com/api/?key=${key}&q=${search}&per_page=${imgPerPage}&page=${actualPage}`;

      const answer = await fetch(url);
      const result = await answer.json();

      saveImg(result.hits);

      // Calcular el total de páginas
      const calculateTotalPages = Math.ceil(result.totalHits / imgPerPage);
      saveTotalPages(calculateTotalPages);

      // Mover la pantalla hacia arriba
      const jumbotron = document.querySelector('.jumbotron');
      jumbotron.scrollIntoView({ behavior: 'smooth' });
    }
    consultApi();

  }, [search, actualPage]);

  // Definir la página anterior
  const previousPage = () => {
    const newActualPage = actualPage - 1;

    if (newActualPage === 0) {
      return;
    }
    saveActualPage(newActualPage);
  }

  // Definir la página siguiente
  const nextPage = () => {
    const newActualPage = actualPage + 1;

    if (newActualPage > totaPages) {
      return;
    }

    saveActualPage(newActualPage);
  }

  return (
    <div className="container">
      <div className="jumbotron">
        <p className="lead text-center">Buscador de imágenes</p>

        <Form
          saveSearch={saveSearch}
        />
      </div>

      <div className="row justify-content-center">
        <ImgList
          img={img}
        />

        {(actualPage === 1) ? null : (
          <button
            type="button"
            className="bbtn btn-info mr-1"
            onClick={previousPage}
          >&laquo; Anterior</button>
        )}

        {(actualPage === totaPages) ? null :
          <button
            type="button"
            className="bbtn btn-info"
            onClick={nextPage}
          >Siguiente &raquo;</button>
        }

      </div>
    </div>
  );
}

export default App;
